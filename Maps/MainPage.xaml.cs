﻿using System;
using System.Threading;
using Maps.Utilities.Helpers.FileReader;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace Maps
{
    public partial class MainPage : ContentPage, iFileConnector
    {
        FileReader fileReader = new FileReader();
        CancellationTokenSource cancellationToken = new CancellationTokenSource();

        public MainPage()
        {
            InitializeComponent();

            fileReader.FileReaderDelegate = this;
            GetData();
        }

        private async void GetData()
        {
            await fileReader.ReadFile("Markers.json", true, cancellationToken.Token);
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Console.WriteLine(jsonData);
        }
    }
}
