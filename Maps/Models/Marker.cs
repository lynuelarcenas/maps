﻿using System;
using Xamarin.Forms.GoogleMaps;

namespace Maps.Models
{
    public class Marker
    {
        public string label { get; set; }
        public string icon { get; set; }
        public PinType type { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
