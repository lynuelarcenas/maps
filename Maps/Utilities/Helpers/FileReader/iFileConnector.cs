﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace Maps.Utilities.Helpers.FileReader
{
    public interface iFileConnector
    {
        void ReceiveJSONData(JObject jsonData, CancellationToken ct);
    }
}
