﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Maps.Utilities.Helpers.FileReader;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace Maps.Views
{
    public partial class MapPage : ContentPage, iFileConnector
    {
        ObservableCollection<Pin> Pokemons = new ObservableCollection<Pin>();
        FileReader fileReader = new FileReader();
        CancellationTokenSource cancellationToken = new CancellationTokenSource();
        Position originalPosition = new Position(10.3001826, 123.898538);

        //Map map;
        public MapPage()
        {
            InitializeComponent();
            //fileReader.FileReaderDelegate = this;
            //GetData();
            NavigationPage.SetHasNavigationBar(this, false);
            //var output = new Label { Text = "" };
            //var img = new Image { Source = "" };
            //map = new Map
            //{
            //    IsShowingUser = true,
            //    HeightRequest = 100,
            //    WidthRequest = 960,
            //    VerticalOptions = LayoutOptions.FillAndExpand
            //};


            map.MoveToRegion(MapSpan.FromCenterAndRadius(originalPosition, Distance.FromMiles(0.1))); // Santa Cruz golf course
           

            var position1 = new Position(10.2998554, 123.8975892);
            var position2 = new Position(10.2996272, 123.8980877);
            var position3 = new Position(10.2993026, 123.8977873);
            var position4 = new Position(10.3003891, 123.8986392);
            var position5 = new Position(10.2996185, 123.8991193);
            var position6 = new Position(10.3001489, 123.8988712);

            var pikachu = new Pin { Type = PinType.Place, Position = position1, Label = "pikachu", Icon = BitmapDescriptorFactory.FromBundle("pikachu.png") };
            var bulbasaur = new Pin { Type = PinType.Place, Position = position2, Label = "bulbasaur", Icon = BitmapDescriptorFactory.FromBundle("bulbasaur.png") };
            var charmander = new Pin { Type = PinType.Place, Position = position3, Label = "charmander", Icon = BitmapDescriptorFactory.FromBundle("charmander.png") };
            var jigglypuff = new Pin { Type = PinType.Place, Position = position4, Label = "jigglypuff", Icon = BitmapDescriptorFactory.FromBundle("jigglypuff.png") };
            var psyduck = new Pin { Type = PinType.Place, Position = position5, Label = "psyduck", Icon = BitmapDescriptorFactory.FromBundle("psyduck.png") };
            var squirtle = new Pin { Type = PinType.Place, Position = position6, Label = "squirtle", Icon = BitmapDescriptorFactory.FromBundle("squirtle.png") };

            Pokemons.Add(pikachu);
            Pokemons.Add(bulbasaur);
            Pokemons.Add(charmander);
            Pokemons.Add(jigglypuff);
            Pokemons.Add(psyduck);
            Pokemons.Add(squirtle);

            //var focus = new Button { Text = "FOCUS" };

            //focus.Clicked += (sender, e) => {
            //    map.MoveToRegion(MapSpan.FromCenterAndRadius(originalPosition, Distance.FromMiles(0.1)));
            //};

            //var buttons = new StackLayout{
            //    Orientation = StackOrientation.Horizontal,
            //    HorizontalOptions = LayoutOptions.CenterAndExpand,
            //    Children = { focus }
            //};

            //map.PinClicked += async (sender, e) =>
            //{
            //    Console.WriteLine(e.Pin);
            //    bool action = await DisplayAlert("Wild " + e.Pin.Label + " Appeared", "What will you do?", "Attack", "Flee");
            //    if (action)
            //    {
            //        await Navigation.PushAsync(new BattlePage());
            //    }
            //};

            //map.PropertyChanged += (sender, e) =>
            //{
            //    if (e.PropertyName == "VisibleRegion")
            //    {
            //        var center = ((Map)sender).VisibleRegion.Center;
            //        var halfheightDegrees = ((Map)sender).VisibleRegion.LatitudeDegrees / 2;
            //        var halfwidthDegrees = ((Map)sender).VisibleRegion.LongitudeDegrees / 2;
            //        double left = center.Longitude - halfwidthDegrees;
            //        double right = center.Longitude + halfwidthDegrees;
            //        double top = center.Latitude + halfheightDegrees;
            //        double bottom = center.Latitude - halfheightDegrees;
            //        if (left < -180)
            //        {
            //            left = 180 + (180 + left);
            //        }
            //        if (right > 180)
            //        {
            //            right = (right - 180) - 180;
            //        }

            //        var temp = new ObservableCollection<Pin>();
            //        foreach (Pin pokemon in Pokemons)
            //        {
            //            if ((pokemon.Position.Latitude > bottom && pokemon.Position.Latitude < top) && (pokemon.Position.Longitude > left && pokemon.Position.Longitude < right))
            //            {
            //                ((Map)sender).Pins.Add(pokemon);
            //                temp.Add(pokemon);
            //                Console.WriteLine("Pokemon Detected: "+pokemon.Label);
            //                //output.Text = "Pokemon detected: "+pokemon.Label;
            //                img.Source = pokemon.Label;      
            //            }
            //            else
            //            {
            //                ((Map)sender).Pins.Remove(pokemon);
            //                temp.Remove(pokemon);
            //                Console.WriteLine("Pokemon Hidden: "+pokemon.Label);
            //            }
            //        }

            //        foreach (Pin pokemon in temp)
            //        {
            //            Pokemons.Remove(pokemon);
            //        }

            //        temp = null;
            //    }
            //};

            //Content = new StackLayout
            //{
            //    Spacing = 0,
            //    Children = {
            //        map,
            //        buttons,
            //        //output,
            //        img

            //    }
            //};



        }

        //private async void GetData()
        //{
        //    await fileReader.ReadFile("Markers.json", true, cancellationToken.Token);
        //}

        void Map_CameraIdled(object sender, Xamarin.Forms.GoogleMaps.CameraIdledEventArgs e)
        {
            double left = ((Map)sender).Region.NearLeft.Longitude;
            double right = ((Map)sender).Region.FarRight.Longitude;
            double top = ((Map)sender).Region.FarRight.Latitude;
            double bottom = ((Map)sender).Region.NearLeft.Latitude;

            if (left < -180) left = 180 + (180 + left);
            if (right > 180) right = (right - 180) - 180;

            var temp = new ObservableCollection<Pin>();

            foreach (Pin pokemon in Pokemons)
            {
                if((pokemon.Position.Latitude > bottom && pokemon.Position.Latitude < top) && (pokemon.Position.Longitude > left && pokemon.Position.Longitude < right))
                {
                    ((Map)sender).Pins.Add(pokemon);
                    Console.WriteLine("Pokemon added: "+ pokemon.Label);
                }
                else
                {
                    ((Map)sender).Pins.Remove(pokemon);
                    Pokemons.Remove(pokemon);
                    Console.WriteLine("Pokemon hidden: " + pokemon.Label);
                }
            }
        }

        async void Pokemon_PinClicked(object sender, Xamarin.Forms.GoogleMaps.PinClickedEventArgs e)
        {
            bool action = await DisplayAlert($"Wild {e.Pin.Label} Appeared", "What will you do?", "Attack", "Flee");

            if (action)
            {
                await Navigation.PushAsync(new BattlePage());
            }
            else
            {
                return;
            }
        }

        void Focus_Clicked(object sender, System.EventArgs e)
        {
            map.MoveToRegion(MapSpan.FromCenterAndRadius(originalPosition, Distance.FromMiles(0.1)));
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            throw new NotImplementedException();
        }
    }
}
